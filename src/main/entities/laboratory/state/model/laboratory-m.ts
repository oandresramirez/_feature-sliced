import Swal from 'sweetalert2';
import { UserData } from "../../domain/model/user-data";
import { RootModel } from "../../../../shared/state/models/root-model";
import { createModel } from "@rematch/core";
//Calculos de enfermedad 

export const laboratoryModel = createModel<RootModel>()({
  state: {
    tasks: []
  },

  effects: dispatch => ({
    async patientValidation(obj: UserData) {
      let save = false;
      try {
        let rta = '';
        if (obj.sugarPercentage > 70 && obj.fatPercentage > 88.5 && obj.oxygenPercentage < 60) {
          alertSuccess('Tienen un riesgo <b>ALTO</b> de enfermar gravemente.');
          save = true;
          rta = 'Tienen un riesgo ALTO de enfermar gravemente.';
          console.log('Tienen un riesgo <b>ALTO</b> de enfermar gravemente.')
        }
        if ((obj.sugarPercentage >= 50 && obj.sugarPercentage <= 70) &&
          (obj.fatPercentage >= 62.2 && obj.fatPercentage <= 88.5) &&
          (obj.oxygenPercentage >= 60 && obj.oxygenPercentage <= 70)
        ) {
          alertSuccess('Tienen un riesgo <b>MEDIO</b> de enfermar gravemente.');
          save = true;
          rta = 'Tienen un riesgo MEDIO de enfermar gravemente.';
          console.log('Tienen un riesgo <b>MEDIO</b> de enfermar gravemente.')
        }
        if (obj.sugarPercentage < 50 && obj.fatPercentage < 62.2 && obj.oxygenPercentage > 70) {
          alertSuccess('Tienen un riesgo <b>BAJO</b> de enfermar gravemente.');
          save = true;
          rta = 'Tienen un riesgo BAJO de enfermar gravemente.';
          console.log('Tienen un riesgo <b>BAJO</b> de enfermar gravemente.')
        }
        if (!save) {
          alertError('No se encontro ninguna enfermedad en el paciente.');
        }
        return {save,rta};
      } catch (error: any) {
        console.log(error);
      }
      return {save,rta:''};
    },
  })
})


const alertError = (text: string) => {
  Swal.fire({
    title: 'Error.',
    html: text,
    icon: 'error',
    showCancelButton: false,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ok'
  });
}

const alertSuccess = (text: string) => {
  Swal.fire({
    title: 'Diagnóstico',
    html: text,
    icon: 'success',
    showCancelButton: false,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ok'
  });
}
