export type UserData = {
    identity: string,
    names: string,
    sugarPercentage: number,
    fatPercentage: number,
    oxygenPercentage: number,
    results: string,
};