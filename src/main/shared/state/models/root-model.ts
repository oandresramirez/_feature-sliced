import { Models } from '@rematch/core'

import { laboratoryModel } from "../../../entities/laboratory/state/model/laboratory-m";

export interface RootModel extends Models<RootModel> {
    laboratoryModel: typeof laboratoryModel
}

export const models: RootModel = { laboratoryModel }