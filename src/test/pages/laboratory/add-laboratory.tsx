import React, { useEffect } from 'react';
import { Formik } from 'formik';
import { UserData } from '../../entities/laboratory/domain/model/user-data'
import * as Yup from 'yup';
import { Dispatch } from "../../shared/state/store";
import { useDispatch } from "react-redux";

const Laboratory: React.FC = () => {

    const dispatch = useDispatch<Dispatch>();
    const userData: UserData = {
        identity: '',
        names: '',
        sugarPercentage: 0,
        fatPercentage: 0,
        oxygenPercentage: 0,
        results: ''
    }
    const [listUserData, setUserData] = React.useState<UserData[]>([]);

    const msj = (value: string) => {
        return `El campo ${value} es obligatorio.`
    }
    let listStoneDisease: UserData[] = [];
    /*     useEffect(() => {
    
        }, [listStoneDisease, listUserData])
     */
    return (
        <div>
            <Formik
                initialValues={userData}
                onSubmit={(values, actions) => {
                    actions.setSubmitting(false);
                    dispatch.laboratoryModel.patientValidation(values).then(data => {
                        if (data.save) {
                            values.results = data.rta;
                           // listStoneDisease.push(values);
                            setUserData([...listUserData,values]);
                            //  { ...formulario, [event.target.name]: event.target.value }
                            console.log('--------->', listUserData);
                        }
                    });
                }}
                validationSchema={
                    Yup.object().shape({
                        identity: Yup
                            .string()
                            .required(msj('Identificación')),
                        names: Yup
                            .string()
                            .required(msj('Nombre completo')),
                        sugarPercentage: Yup
                            .number().positive('No se admiten valores negativos.')
                            .required(msj('Porcentaje de azúcar'))
                            .max(100, "Porcentaje de azúcar no puede ser mayor a 100."),
                        fatPercentage: Yup
                            .number().positive('No se admiten valores negativos.')
                            .required(msj('Porcentaje de grasar'))
                            .max(100, "Porcentaje de grasarno puede ser mayor a 100."),
                        oxygenPercentage: Yup
                            .number().positive('No se admiten valores negativos.')
                            .required(msj('Porcentaje de oxígeno'))
                            .max(100, "Porcentaje de oxígeno no puede ser mayor a 100."),
                    })
                }>

                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        handleChange,
                        handleBlur,
                        handleSubmit
                    } = props;
                    return (
                        <form
                            noValidate
                            autoComplete="off"
                            onSubmit={handleSubmit}
                        >
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-12 text-center">
                                        <h1>Estudio de sangre</h1>
                                    </div>
                                    <div className="shadow p-3 mb-5 bg-white rounded _card row">
                                        <div className="col-md-6 mb-2">
                                            <input
                                                type="text"
                                                name="identity"
                                                className="form-control"
                                                placeholder="Identificación"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.identity} />
                                            {errors.identity && touched.identity && (
                                                <div
                                                    className="input-feedback"
                                                    data-testid="errorIdentity"
                                                >{errors.identity}</div>
                                            )}
                                        </div>
                                        <div className="col-md-6 mb-2">
                                            <input
                                                type="text"
                                                name="names"
                                                className="form-control"
                                                placeholder="Nombre completo"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.names} />
                                            {errors.names && touched.names && (
                                                <div
                                                    className="input-feedback"
                                                    data-testid="errorIdentity">
                                                    {errors.names}</div>
                                            )}
                                        </div>
                                        <div className="col-md-6 mb-2">
                                            <input
                                                type="number"
                                                name="sugarPercentage"
                                                className="form-control"
                                                placeholder="Porcentaje de azúcar"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.sugarPercentage} />
                                            {errors.sugarPercentage && touched.sugarPercentage && (
                                                <div
                                                    className="input-feedback"
                                                    data-testid="errorIdentity">
                                                    {errors.sugarPercentage}</div>
                                            )}
                                        </div>
                                        <div className="col-md-6 mb-2">
                                            <input
                                                type="number"
                                                name="fatPercentage"
                                                className="form-control"
                                                placeholder="Porcentaje de grasa"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.fatPercentage} />
                                            {errors.fatPercentage && touched.fatPercentage && (
                                                <div
                                                    className="input-feedback"
                                                    data-testid="errorIdentity">
                                                    {errors.fatPercentage}</div>
                                            )}
                                        </div>
                                        <div className="col-md-12 mb-2">
                                            <input
                                                type="number"
                                                name="oxygenPercentage"
                                                className="form-control"
                                                placeholder="Porcentaje de oxígeno"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.oxygenPercentage} />
                                            {errors.oxygenPercentage && touched.oxygenPercentage && (
                                                <div
                                                    className="input-feedback"
                                                    data-testid="errorIdentity">
                                                    {errors.oxygenPercentage}</div>
                                            )}
                                        </div>
                                        <div className="col-md-12 mb-2 text-center">
                                            <button type="submit" className="btn btn-primary">EXAMINAR</button>
                                        </div>
                                    </div>

                                    <div className="shadow p-3 mb-5 bg-white rounded row">
                                        <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Identificación</th>
                                                    <th>Nombre completo</th>
                                                    <th>Porcentaje de azúcar</th>
                                                    <th>Porcentaje de grasa</th>
                                                    <th>Porcentaje de oxígeno</th>
                                                    <th>Examen</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {(listUserData.length!==0) &&
                                                    listUserData.map((data, index) =>
                                                        <tr key={index}>
                                                            <td>{data.identity}</td>
                                                            <td>{data.names}</td>
                                                            <td>{data.sugarPercentage}</td>
                                                            <td>{data.fatPercentage}</td>
                                                            <td>{data.oxygenPercentage}</td>
                                                            <td>{data.results}</td>
                                                        </tr>
                                                    )
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {/* <input
                                type="password"
                                name="password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.password}
                            />
                            {errors.password && touched.password && errors.password} */}
                        </form>
                    );
                }}
            </Formik>
        </div >
    )
};

export default Laboratory;