
import React from "react";
import { Route, Routes, Navigate } from "react-router-dom";

import Laboratory from "./laboratory/add-laboratory";

export const Routing: React.FC = () => {
    return (
        <Routes>
            <Route path="/" element={ <Laboratory /> } />
            <Route  path="*" element={ <Navigate to="/" /> } /> 
        </Routes>
    );
};